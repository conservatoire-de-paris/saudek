# Max 2.0aplha40 Tables

Early versions of Miller Pucket's _Max_ seemed to use two file formats to store values from _table_ objects. One of them is a simple ASCII format which is still being used today, while the other one was a binary format which recent versions of _Max_ can't read anymore.

While attempting to revive a work from Vojtech Saudek, such files were re-discovered and reverse-engineered in order to write a simple conversion utility.

Mathieu Demange (mdemange@cnsmdp.fr)

## Binary format

**Important:** this binary file format was used by a native m68k app so everything is **big-endian** here. 

### Structure

    OFFSET               SIZE            SHORT NAME      DESCRIPTION

    0000                 unsigned int    VER             Always 0x00000001: probably a version hint.
    
    0004                 unsigned int    NUM_VALUES      Number of values in the table (e.g. 0x00000036).
    
    0008                 unsigned int    DATA_SIZE       Data block size
                                                         Sshould be equal to NUM_VALS * 4, e.g. 0x000000D8.
    
    000C                 unsigned char   ???             0x03 followed by (NUM_VALUES - 1) times 0x01.
                                                         Probably carries some info about the values.
    
    000C + NUM_VALUES    unsigned int    VALUES          Table value steps.
    
    EOF  - 06x00         char[6]         MAGIC           Contains: 'table\0'.

### Example

    00000001 00000036 000000D8 03010101 01010101 01010101 01010101 01010101
    01010101 01010101 01010101 01010101 01010101 01010101 01010101 01010101
    01010000 00000000 002B0000 002C0000 002D0000 002E0000 002F0000 00300000
    00310000 00320000 00330000 00340000 00350000 00360000 00370000 00380000
    00390000 003A0000 003B0000 003C0000 003D0000 003E0000 003F0000 00400000
    00410000 00420000 00430000 00440000 00450000 00460000 00470000 00480000
    00490000 004A0000 004B0000 004C0000 004D0000 004E0000 004F0000 00500000
    00510000 00520000 00530000 00540000 00550000 00560000 00570000 00580000
    00590000 005A0000 005B0000 005C0000 005D0000 005E0000 005F7461 626C6500

## ASCII format

### Structure

`table ` followed by ASCII numerical representation of each table value, separated by a whitespace.

### Example

    table 43 55 62 67 71 74 77 79 81 83 81 79 77 74 71 67 62 55 43 55 62 67 71 74 77 79 81 83 85 86 88 93

## Conversion utility

Based on the above information, a Python script was written to convert binary table files to ASCII.

Usage: `convert_table [-h] source destination`

It can also be used to batch process files:

    cd src_tables
    find . -exec ../convert_table {} ../dest_tables/{} \;