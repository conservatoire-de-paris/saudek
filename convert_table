#!/usr/bin/env python3

# Mathieu Demange (mdemange@cnsmdp.fr)

import struct
import argparse
import os

parser = argparse.ArgumentParser (description='Convert an old Max table value set stored in binary format to ASCII format.')
parser.add_argument ('source', type=argparse.FileType ('rb'))
parser.add_argument ('destination', type=argparse.FileType ('w'))

args = parser.parse_args()

with args.source as file:

    print ("Converting '" + file.name + "'... ", end="")

    if file.read (5) == b"table":
        print("Table is already in ASCII format.")
        file.seek (0, os.SEEK_END)
        size = file.tell()
        file.seek (0)
        args.destination.write (file.readlines()[0].decode())
        quit (1)

    file.seek (-6, 2)
    if file.read (6) != b"table\x00":
        print ("Unexpected file format.")
        quit (2)

    file.seek (0)
    version,    = struct.unpack ('>I', file.read (4))
    num_values, = struct.unpack ('>I', file.read (4))
    data_size,  = struct.unpack ('>I', file.read (4))
    file.read (num_values)
    values = [struct.unpack ('>I', file.read (4))[0] for i in range (0, num_values)]

    args.destination.write ("table " + " ".join ([str (v) for v in values]))
    args.destination.close()
    print ("Done.")
    quit (0)